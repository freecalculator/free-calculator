import { CalculatorActionType } from "../actions/CalculatorActions";
import { ITimeFrameProps } from "../components/TimeFrame/TimeFrame";
import { combineReducers } from "redux";
import { IPeriod } from "../models/IPeriod";

function round(value: number) {
    return Number(Math.round(value*100)/100);
}

function calculateTimeFrame(index: number, initialInvestment: number, timeFrame: ITimeFrameProps, lastPeriod: number, lastPeriodInt: number, totalInvestment: number): IPeriod[] {
    // DEVELOPERS NOTE:
    // Here we are assigning the variables to the old code with our classes
    // code so we can remove old jQuery code dealing with GUI. We also will
    // declare a few new variables for our new class.
    var result: IPeriod[] = [];
    var interestGraphData = [];
    var totalValueGraphData = [];
    var initial = +initialInvestment;
    var addition = +(timeFrame.addition || 0);
    var years = +(timeFrame.years || 1);
    var interest = round(+(timeFrame.interest || 0)/100);
    var aPower = +(timeFrame.regularity || 52);
    var mPower = +(timeFrame.interestRegularity || 12);
    var currentPeriod = 1;
    var totalPeriod;

    // DEVELOPERS NOTE:
    // The code below is copy and pasted here from the previous project.
    // We have only modified how the data is saved, nothing else here is
    // modified from the old code.

    if (aPower == 26 || aPower == 52)
        totalPeriod = years * 52;
    else
        totalPeriod = years * 12;

    var currentInvestment = initial;
    var periodInterest = 0;
    var totalInterest = lastPeriodInt;
    var totalValue;

    var graphplots = new Array();
    var graphmonths = new Array();
    var graphinterest = new Array();

    var effectiveInt = Math.pow((1+(interest/mPower)), mPower) - 1;
    var monthlyInt = Math.pow((1 + effectiveInt), (1/12)) - 1;
    var weeklyInt = Math.pow((1 + effectiveInt), (1/52)) - 1;;

    var depositFlag = 0;
    var table52Counter = 0;
    var tablenum = 0;
    if (aPower == 52){
        depositFlag = 1;
        table52Counter = Math.floor(aPower/mPower);
        tablenum = 1;
    }
    else if (aPower == 26){
        depositFlag = 2;
        table52Counter = Math.floor(aPower/mPower * 2);
        tablenum = 1;
    }
    else if (aPower == 12)
        depositFlag = 1;
    else if (aPower == 4)
        depositFlag = 3;
    else if (aPower == 2)
        depositFlag = 6;
    else if (aPower == 1)
        depositFlag = 12;

    var tableFlag = 0;
    if (mPower == 12)
        tableFlag = 1;
    else if (mPower == 4)
        tableFlag = 3
    else if (mPower == 2)
        tableFlag = 6
    else if (mPower == 1)
        tableFlag = 12;

    graphplots[0] = Number(initial).toFixed(2);
    graphmonths[0] = 1;
    graphinterest[0] = 0;

    var deposithere = 0;

    while (currentPeriod <= totalPeriod) {
        deposithere = currentPeriod%depositFlag;
        if (aPower < 26){
            periodInterest = currentInvestment * monthlyInt;
            totalInterest += periodInterest;

            currentInvestment += periodInterest;
            if (deposithere == 0){
                totalInvestment += addition;
                currentInvestment += addition
            }

            tablenum = currentPeriod/tableFlag;

            totalValue = totalInvestment + totalInterest;

            if (currentPeriod%tableFlag == 0){
                var currentPeriodInt = totalInterest - lastPeriodInt;
                lastPeriodInt = totalInterest;

                var currentPeriodInvestment = totalInvestment - lastPeriod;
                lastPeriod = totalInvestment;

                // DEVELOPERS NOTE:
                // (1) Here is where we comment out the old code of how the compounding events
                //     are saved and save them to our new data array.
                result.push({
                    Index: index,
                    Period: tablenum,
                    PeriodInvestment: round(currentPeriodInvestment),
                    TotalInvestment: round(totalInvestment),
                    PeriodInterest: round(currentPeriodInt),
                    TotalInterest: round(totalInterest),
                    TotalValue: round(totalValue)
                });
                // amort+='<tr><td> ' + tablenum + '</td><td>$' + formatMoney(currentPeriodInvestment, 2, '.', ',') + '</td><td>$' + formatMoney(totalInvestment, 2, '.', ',') + '</td><td>$' + formatMoney(currentPeriodInt, 2, '.', ',') + '</td><td>$' + formatMoney(totalInterest, 2, '.', ',') + '</td><td>$' + formatMoney(totalValue, 2, '.', ',') + '</td></tr>';
                // printamort+='<tr><td> ' + tablenum + '</td><td>$' + formatMoney(currentPeriodInvestment, 2, '.', ',') + '</td><td>$' + formatMoney(totalInvestment, 2, '.', ',') + '</td><td>$' + formatMoney(currentPeriodInt, 2, '.', ',') + '</td><td>$' + formatMoney(totalInterest, 2, '.', ',') + '</td><td>$' + formatMoney(totalValue, 2, '.', ',') + '</td></tr>';

                // DEVELOPERS NOTE:
                // (2) Here is how we save our graph.
                interestGraphData.push(round(totalInterest));
                totalValueGraphData.push(round(totalValue));
                // graphinterest.push(totalInterest);
                //     graphplots.push(totalValue);
            }
        }
        else{
            periodInterest = currentInvestment * weeklyInt;
            totalInterest += periodInterest;

            currentInvestment += periodInterest;
            if (aPower == 52){
                totalInvestment += addition;
                currentInvestment += addition
            } else {
                if (currentPeriod%2 == 0){
                    totalInvestment += addition;
                    currentInvestment += addition
                }
            }

            totalValue = totalInvestment + totalInterest;
            if (currentPeriod%(table52Counter) == 0){
                var currentPeriodInt = totalInterest - lastPeriodInt;
                lastPeriodInt = totalInterest;

                var currentPeriodInvestment = totalInvestment - lastPeriod;
                lastPeriod = totalInvestment;

                // DEVELOPERS NOTE:
                // (1) Here is where we comment out the old code of how the compounding events
                //     are saved and save them to our new data array.
                result.push({
                    Index: index,
                    Period: tablenum,
                    PeriodInvestment: round(currentPeriodInvestment),
                    TotalInvestment: round(totalInvestment),
                    PeriodInterest: round(currentPeriodInt),
                    TotalInterest: round(totalInterest),
                    TotalValue: round(totalValue)
                });
                // amort+='<tr><td> ' + tablenum + '</td><td>$' + formatMoney(currentPeriodInvestment, 2, '.', ',') + '</td><td>$' + formatMoney(totalInvestment, 2, '.', ',') + '</td><td>$' + formatMoney(currentPeriodInt, 2, '.', ',') + '</td><td>$' + formatMoney(totalInterest, 2, '.', ',') + '</td><td>$' + formatMoney(totalValue, 2, '.', ',') + '</td></tr>';
                // printamort+='<tr><td> ' + tablenum + '</td><td>$' + formatMoney(currentPeriodInvestment, 2, '.', ',') + '</td><td>$' + formatMoney(totalInvestment, 2, '.', ',') + '</td><td>$' + formatMoney(currentPeriodInt, 2, '.', ',') + '</td><td>$' + formatMoney(totalInterest, 2, '.', ',') + '</td><td>$' + formatMoney(totalValue, 2, '.', ',') + '</td></tr>';

                tablenum ++; // Increment the tablenum.
            }


        }
        currentPeriod++;

    }
    return result;
}

function calculateTimeFrames(initialInvestment: number, timeFrames: ITimeFrameProps[]) {
    var result: IPeriod[] = [];
    var init: number = initialInvestment;
    for (var i=0; i<timeFrames.length; i++) {
        result = result.concat(calculateTimeFrame(i+1, init, timeFrames[i], i === 0 ? 0 : result[result.length-1].TotalInvestment, i === 0 ? 0 : result[result.length-1].TotalInterest, i === 0 ? +initialInvestment : result[result.length-1].TotalInvestment));
        init = result[result.length-1].TotalValue;
    }
    return result;
}

export function CalculationReducer(state: any[] = [], action: any) {
    switch (action.type) {
        case CalculatorActionType.CALCULATE:
            return calculateTimeFrames(action.initialInvestment, action.timeFrames);
        default:
            return state;
    }
}

export function InitialInvestmentReducer(state: number = 0, action: any) {
    switch (action.type) {
        case CalculatorActionType.CHANGE_INITIAL_INVESTMENT:
            return action.initialInvestment;
        default:
            return state;
    }
}

export function TimeFramesReducer(state: ITimeFrameProps[] = [{}], action: any) {
    switch (action.type) {
        case CalculatorActionType.ADD_TIMEFRAME:
            return [
                ...state,
                {}
            ]
        case CalculatorActionType.CHANGE_ADDITION:
            return state.map((timeFrame, index) => {
                if (index === action.index) {
                    return Object.assign({}, timeFrame, {
                        addition: action.addition
                    })
                }

                return timeFrame;
            })
        case CalculatorActionType.CHANGE_REGULARITY:
            return state.map((timeFrame, index) => {
                if (index === action.index) {
                    return Object.assign({}, timeFrame, {
                        regularity: action.regularity
                    })
                }

                return timeFrame;
            })
        case CalculatorActionType.CHANGE_INTEREST:
            return state.map((timeFrame, index) => {
                if (index === action.index) {
                    return Object.assign({}, timeFrame, {
                        interest: action.interest
                    })
                }

                return timeFrame;
            })
        case CalculatorActionType.CHANGE_INTEREST_REGULARITY:
            return state.map((timeFrame, index) => {
                if (index === action.index) {
                    return Object.assign({}, timeFrame, {
                        interestRegularity: action.interestRegularity
                    })
                }

                return timeFrame;
            })
        case CalculatorActionType.CHANGE_YEARS:
            return state.map((timeFrame, index) => {
                if (index === action.index) {
                    return Object.assign({}, timeFrame, {
                        years: action.years
                    })
                }

                return timeFrame;
            })
        default:
            return state;
    }
}

const CalculatorApp = combineReducers({
    InitialInvestmentReducer,
    TimeFramesReducer, 
    CalculationReducer
});

export default CalculatorApp;