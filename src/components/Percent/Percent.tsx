import TextField from '@material-ui/core/TextField';
import * as React from 'react';
import NumberFormat from 'react-number-format';

function NumberFormatCustom(props: any){
  const { inputRef, onChange, min, max, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      isAllowed={values => {
          return +values.value >= min && +values.value <= max;
      }}
      onValueChange={values => {
        onChange({
            target: {
                value: values.value,
            },
        });
      }}
      suffix=" %"
    />
  );
}

function Percent(props: any) {
  const { label, min, max, value, onChange, ...other } = props;

  return (
    <TextField 
        {...other}
        label={label}
        value={value}
        onChange={(event) => { 
          onChange(event.target.value);
      }}
        InputProps={{
            inputComponent: NumberFormatCustom,
            inputProps: { min: min, max: max }
        }}
    />
  );
}

export default Percent;