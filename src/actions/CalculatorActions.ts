import { ITimeFrameProps } from "../components/TimeFrame/TimeFrame";

export enum CalculatorActionType {
    CHANGE_INITIAL_INVESTMENT = "CHANGE_INITIAL_INVESTMENT",
    ADD_TIMEFRAME = "ADD_TIMEFRAME",
    CALCULATE = "CALCULATE",
    CHANGE_ADDITION = "CHANGE_ADDITION", 
    CHANGE_REGULARITY = "CHANGE_REGULARITY",
    CHANGE_INTEREST = "CHANGE_INTEREST",
    CHANGE_INTEREST_REGULARITY = "CHANGE_INTEREST_REGULARITY",
    CHANGE_YEARS = "CHANGE_YEARS"
}

export function changeInitialInvestment(initialInvestment: number) {
    return {
        initialInvestment,
        type: CalculatorActionType.CHANGE_INITIAL_INVESTMENT
    };
}

export function addTimeFrame() {
    return {
        type: CalculatorActionType.ADD_TIMEFRAME
    }
}

export function calculate(initialInvestment: number, timeFrames: ITimeFrameProps[]){
    return {
        initialInvestment, 
        timeFrames,
        type: CalculatorActionType.CALCULATE
    }
}

export function changeAddition(index:number, addition: number) {
    return {
        index,
        addition,
        type: CalculatorActionType.CHANGE_ADDITION
    };
}

export function changeRegularity(index:number, regularity: number) {
    return {
        index,
        regularity,
        type: CalculatorActionType.CHANGE_REGULARITY
    };
}

export function changeInterest(index:number, interest: number) {
    return {
        index,
        interest,
        type: CalculatorActionType.CHANGE_INTEREST
    };
}

export function changeInterestRegularity(index:number, interestRegularity: number) {
    return {
        index,
        interestRegularity,
        type: CalculatorActionType.CHANGE_INTEREST_REGULARITY
    };
}

export function changeYears(index:number, years: number) {
    return {
        index,
        years,
        type: CalculatorActionType.CHANGE_YEARS
    };
}