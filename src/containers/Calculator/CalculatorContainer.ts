import { connect } from 'react-redux'
import Calculator from "../../components/Calculator/Calculator";
import { changeInitialInvestment, addTimeFrame, changeAddition, changeRegularity, changeInterest, changeInterestRegularity, changeYears, calculate } from "../../actions/CalculatorActions";
import { ITimeFrameProps } from '../../components/TimeFrame/TimeFrame';

const mapStateToProps = (state: any) => {
    return {
        initialInvestment: state.InitialInvestmentReducer,
        timeFrames: state.TimeFramesReducer, 
        calculation: state.CalculationReducer
    }
  }

const mapDispatchToProps = (dispatch: any) => {
    return {
        onChangeInitialInvestment: (initialInvestment: number) => {
            dispatch(changeInitialInvestment(initialInvestment));
        },
        onAddTimeFrame: () => {
            dispatch(addTimeFrame());
        },
        onCalculate: (initialInvestment: number, timeFrames: ITimeFrameProps[]) => {
            dispatch(calculate(initialInvestment, timeFrames));
        }, 
        onChangeAddition: (index:number, addition: number) => {
            dispatch(changeAddition(index, addition));
        }, 
        onChangeRegularity: (index:number, regularity: number) => {
            dispatch(changeRegularity(index, regularity));
        }, 
        onChangeInterest: (index:number, interest: number) => {
            dispatch(changeInterest(index, interest));
        }, 
        onChangeInterestRegularity: (index:number, interestRegularity: number) => {
            dispatch(changeInterestRegularity(index, interestRegularity));
        },
        onChangeYears: (index:number, years: number) => {
            dispatch(changeYears(index, years));
        }
    }
}
​
const CalculatorContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Calculator)
​
export default CalculatorContainer;