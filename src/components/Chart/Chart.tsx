import * as React from 'react';
import { AreaChart, CartesianGrid, XAxis, YAxis, Area, Tooltip } from 'recharts';

function Chart(props: any) {
    const { data } = props;

    if (data == null || data.length == 0) return null;
  
    return (
        <AreaChart width={800} height={400} data={data} margin={{top: 10, right: 10, left: 0, bottom: 0}}>
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis dataKey="Period"/>
            <YAxis dataKey="TotalValue" />
            <Tooltip />
            <Area type='monotone' dataKey='TotalValue'  fill='#8884d8' />
            <Area type='monotone' dataKey='TotalInterest' fill='#82ca9d' />
        </AreaChart>
    );
  }
  
  export default Chart;