import * as React from 'react';
import Money from '../Money/Money';
import TimeFrame, { ITimeFrameProps } from '../TimeFrame/TimeFrame';
import { Card, CardContent, withStyles, Stepper, Step, StepButton, Button, Tabs, Tab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Chart from '../Chart/Chart';
import Grid from '../Grid/Grid';

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center',
        marginBottom: 10,
        marginTop: 5,
    },
    card: {
      width: 500,
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      marginBottom: 16,
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  };

export interface ICalculatorProps {
    initialInvestment?: number;
    timeFrames?: ITimeFrameProps[];
    onChangeInitialInvestment?: (initialInvestment: number) => void;
    onAddTimeFrame?: () => void;
    onCalculate?: (initialInvestment: number, timeFrames: ITimeFrameProps[]) => void;
    onChangeAddition?: (index: number, addition: number) => void;
    onChangeRegularity?: (index:number, regularity: number) => void;
    onChangeInterest?: (index:number, interest: number) => void;
    onChangeInterestRegularity?: (index:number, interestRegularity: number) => void;
    onChangeYears?: (index:number, years: number) => void;
    calculation?: any[];
}

export interface ICalculatorState {
    activeStep: number;
    activeTab: number;
}

class Calculator extends React.Component<ICalculatorProps, ICalculatorState> {

    state = {
        activeStep: 0,
        activeTab: 0
    };

    handleStep = (step: any) => () => {
        this.setState({
            activeStep: step
        });
    };

    handleChange = (event: any, value: any) => {
        this.setState({ 
            activeTab: value 
        });
    };

    public render() {
        const {
            timeFrames = [{}], 
            initialInvestment = 0, 
            onChangeInitialInvestment, 
            onAddTimeFrame, 
            onCalculate,
            onChangeAddition, 
            onChangeRegularity, 
            onChangeInterest, 
            onChangeInterestRegularity,
            onChangeYears,
            calculation
        } = this.props;
        const timeFrame = timeFrames[this.state.activeStep];
        let calcResult;
        if (calculation && calculation.length > 0) {
            calcResult = (<div style={{ width: 900}}>
                <Tabs value={this.state.activeTab} onChange={this.handleChange}>
                    <Tab label="Table" />
                    <Tab label="Chart" />
                </Tabs>
                {this.state.activeTab === 0 && <Grid data={calculation} />}
                {this.state.activeTab === 1 && <Chart data={calculation} />}
            </div>);
        }

        return (
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <div style={styles.container}>
                    <Card style={{width: 275}}>
                        <CardContent>
                            <Money label="Initial Investment" value={initialInvestment} onChange={onChangeInitialInvestment} />
                        </CardContent>
                    </Card>
                </div>
                <div style={styles.container}>
                    <Card style={{width: 800}}>
                        <CardContent>
                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                                <Stepper>
                                    {timeFrames.map((label, index) => {
                                        return (
                                            <Step key={index} active disabled={false}>
                                                <StepButton onClick={this.handleStep(index)}><span style={ index == this.state.activeStep ? {fontWeight: 'bold'} : {}}>{"Step " + (index + 1)}</span></StepButton>
                                            </Step>
                                        );
                                    })}
                                </Stepper>
                                <div>
                                    <TimeFrame 
                                        addition={timeFrame.addition || 0} 
                                        interest={timeFrame.interest || 0}
                                        regularity={timeFrame.regularity || 52}
                                        interestRegularity={timeFrame.interestRegularity || 12}
                                        years={timeFrame.years || 1}
                                        handleChangeAddition={(addition: number) => { if (onChangeAddition) onChangeAddition(this.state.activeStep, addition)}}
                                        handleChangeRegularity={(regularity: number) => { if (onChangeRegularity) onChangeRegularity(this.state.activeStep, regularity) }}
                                        handleChangeInterest={(interest: number) => { if (onChangeInterest) onChangeInterest(this.state.activeStep, interest)}}
                                        handleChangeInterestRegularity={(interestRegularity: number) => { if (onChangeInterestRegularity) onChangeInterestRegularity(this.state.activeStep, interestRegularity) }}
                                        handleChangeYears={(years: number) => { if (onChangeYears) onChangeYears(this.state.activeStep, years) }}
                                    />
                                </div>
                            </div>
                        </CardContent>
                    </Card>
                </div>
                <div>
                    <Button style={{marginLeft: 10, marginRight: 10}} disabled={timeFrames.length > 3} variant="fab" color="primary" aria-label="Add" onClick={onAddTimeFrame}>
                        <AddIcon />
                    </Button>
                    <Button style={{marginLeft: 10, marginRight: 10}} variant="fab" aria-label="Calculate" onClick={() => { if (onCalculate) onCalculate(initialInvestment, timeFrames) }}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M3.5 18.49l6-6.01 4 4L22 6.92l-1.41-1.41-7.09 7.97-4-4L2 16.99z"/>
                            <path fill="none" d="M0 0h24v24H0z"/>
                        </svg>
                    </Button>
                </div>
                {calcResult}
            </div>
        );
    }
}

export default withStyles(styles)(Calculator);