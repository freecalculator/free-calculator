import * as React from 'react';
import './App.css';

import logo from './logo.svg';
import { Provider } from 'react-redux';
import CalculatorApp from './reducers/CalculatorReducers';
import { createStore } from 'redux';
import CalculatorContainer from './containers/Calculator/CalculatorContainer';

class App extends React.Component {
  public render() {
    const store = createStore(CalculatorApp);
    window['store'] = store;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Timeframe Investment Calculator</h1>
        </header>
        <div>
          <Provider store={store}>
            <CalculatorContainer />
          </Provider>
        </div>
      </div>
    );
  }
}

export default App;
