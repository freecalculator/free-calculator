import * as React from 'react';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import Money from '../Money/Money';
import Percent from '../Percent/Percent';
import NumberOfYears from '../NumberOfYears/NumberOfYears';

export interface ITimeFrame {
    title: string;
}

export interface ITimeFrameProps {
    initialInvesment?: number;
    addition?: number;
    regularity?: number;
    interest?: number;
    interestRegularity?: number;
    years?:number;
    handleChangeAddition?: (event: any) => void;
    handleChangeRegularity?: (event: any) => void;
    handleChangeInterest?: (event: any) => void;
    handleChangeInterestRegularity?: (event: any) => void;
    handleChangeYears?: (event: any) => void;
}

const TimeFrame = (props: ITimeFrameProps) => (
    <div>
        <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', marginBottom: 10}}>
            <Money style={{width: 100, marginRight: 10}} label="Addition" value={props.addition || ""} onChange={props.handleChangeAddition} />
            <FormControl style={{width: 150}}>
                <InputLabel htmlFor="regularity">Frequency</InputLabel>
                <Select
                    value={(props.regularity != null ? props.regularity : 1)}
                    onChange={(event) => {
                        if (props.handleChangeRegularity) props.handleChangeRegularity(event.target.value);
                    }}
                    inputProps={{
                        name: 'regularity',
                        id: 'regularity',
                    }}>
                    <MenuItem value={52}>Weekly</MenuItem>
                    <MenuItem value={26}>Bi-Weekly</MenuItem>
                    <MenuItem value={12}>Monthly</MenuItem>
                    <MenuItem value={4}>Quarterly</MenuItem>
                    <MenuItem value={2}>Semi-annually</MenuItem>
                    <MenuItem value={1}>Yearly</MenuItem>
                </Select>
            </FormControl>
        </div>
        <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', marginBottom: 10}}>
            <Percent style={{width: 100, marginRight: 10}} label="Interest rate" min={0} max={100} value={props.interest || ""} onChange={props.handleChangeInterest} />
            <FormControl style={{width: 150}}>
                <InputLabel htmlFor="interest-regularity">Compounded</InputLabel>
                <Select
                    value={(props.interestRegularity != null ? props.interestRegularity : 4)}
                    onChange={(event) => {
                        if (props.handleChangeInterestRegularity) props.handleChangeInterestRegularity(event.target.value);
                    }}
                    inputProps={{
                        name: 'interest-regularity',
                        id: 'interest-regularity',
                    }}>
                    <MenuItem value={12}>Monthly</MenuItem>
                    <MenuItem value={4}>Quarterly</MenuItem>
                    <MenuItem value={2}>Semi-annually</MenuItem>
                    <MenuItem value={1}>Yearly</MenuItem>
                </Select>
            </FormControl>
        </div>
        <div>
            <NumberOfYears style={{width: 100}} label="Years to grow (1-60)" min={1} max={60} value={props.years || ""} onChange={props.handleChangeYears} />
        </div>
    </div>
);

export default TimeFrame;