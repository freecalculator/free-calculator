import * as React from 'react';
import { IPeriod } from '../../models/IPeriod';
import { withStyles } from '@material-ui/core/styles';
import { Table, TableRow, TableCell, TableBody, IconButton, Paper, TableFooter, TablePagination, TableHead } from '@material-ui/core';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const actionsStyles = (theme: any) => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});


interface IGridPaginationActionsProps {
  onChangePage: any, 
  page: number, 
  count: number, 
  rowsPerPage: number,
  classes: any, 
  theme: any
}

class GridPaginationActions extends React.Component<IGridPaginationActionsProps> {
  handleFirstPageButtonClick = (event: any) => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = (event: any) => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = (event: any) => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = (event: any) => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };

  render() {
    const { count, page, rowsPerPage, theme, classes } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme && theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme && theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme && theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme && theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

const GridPaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  GridPaginationActions,
);

const styles = (theme: any) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
  },  
});

class Grid extends React.Component<{data: any, classes: any}> {
  state = {
    page: 0,
    rowsPerPage: 5,
  };

  handleChangePage = (event: any, page: number) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = (event: any) => {
    this.setState({ rowsPerPage: event.target.value });
  };

  render() {
    if (this.props.data == null || this.props.data.length == 0) return null;

    const { data, classes } = this.props;
    const { rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <div style={{overflowX: 'auto'}}>
          <Table className={classes.table} style={{tableLayout: 'auto'}}>
            <TableHead>
              <TableRow>
                <TableCell colSpan={1}>Step</TableCell>
                <TableCell colSpan={1}>Period</TableCell>
                <TableCell numeric>Period Investment</TableCell>
                <TableCell numeric>Total Investment</TableCell>
                <TableCell numeric>Period Interest</TableCell>
                <TableCell numeric>Total Interest</TableCell>
                <TableCell numeric>Total Value</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((item: IPeriod) => {
                return (
                  <TableRow key={item.Index.toString() + "-" +  item.Period.toString()}>
                        <TableCell colSpan={1} component="th" scope="row">{item.Index}</TableCell>
                        <TableCell colSpan={1} numeric>{item.Period}</TableCell>
                        <TableCell numeric>{item.PeriodInvestment}</TableCell>
                        <TableCell numeric>{item.TotalInvestment}</TableCell>
                        <TableCell numeric>{item.PeriodInterest}</TableCell>
                        <TableCell numeric>{item.TotalInterest}</TableCell>
                        <TableCell numeric>{item.TotalValue}</TableCell>
                    </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 48 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  colSpan={3}
                  count={data.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={GridPaginationActionsWrapped}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </Paper>
    );
  }
}
  
export default withStyles(styles)(Grid);