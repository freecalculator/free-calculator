import TextField from '@material-ui/core/TextField';
import * as React from 'react';
import NumberFormat from 'react-number-format';

function NumberFormatCustom(props: any){
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      prefix="$"
    />
  );
}

function Money(props: any) {
  const { label, value, onChange, ...other } = props;

  return (
    <TextField 
        {...other}
        label={label}
        value={value}
        onChange={(event) => { 
            onChange(event.target.value);
        }}
        InputProps={{
            inputComponent: NumberFormatCustom
        }}
    />
  );
}

export default Money;