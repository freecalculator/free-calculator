export interface IPeriod {
    Index: number,
    Period: number, 
    PeriodInvestment: number, 
    TotalInvestment: number, 
    PeriodInterest: number, 
    TotalInterest: number, 
    TotalValue: number
}